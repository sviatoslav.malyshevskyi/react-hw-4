import React, { useContext } from 'react';
import './Buttons.css';
import StartButton from "./StartButton";
import ResetButton from "./ResetButton";
import StopButton from "./StopButton";

const Buttons = () => {

  return (
      <div className="buttons-container">
        <StartButton />
        <StopButton />
        <ResetButton />
      </div>
  );
};

export default Buttons;
