const initialState = {
  sec: '00',
  msec: '00',
  isCounting: false,
  interval: null
};

export default initialState;
