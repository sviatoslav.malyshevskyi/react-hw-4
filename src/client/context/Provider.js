import React , { useReducer } from 'react';
import reducer from "./reducer";
import context from "./context";
import initialState from "./initialState";
import { START, STOP, RESET } from "./constants";

const Provider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  let {interval} = state;

  const value = {
    state,

    startCount: () => {
      if (state.isCounting === false) {
        interval = setInterval(() => dispatch({type: START, payload: interval}), 10)}},

    stopCount: () => {clearInterval(interval);
    dispatch({type: STOP})},

    resetCount: () => {clearInterval(interval);
      dispatch({type: RESET})},
  };

  return (
      <context.Provider value={value}>
        {children}
      </context.Provider>
  );
};

export default Provider;
