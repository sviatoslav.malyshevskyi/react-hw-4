import React, { useContext } from 'react';
import context from "../../context/context";
import './Stopwatch.css';

const Stopwatch = () => {
  const {sec, msec} = useContext(context).state;

  return (
      <>
        <p className="stopwatch"><span id="sec">{sec}</span>:<span id="msec">{msec}</span></p>
      </>
  );
};

export default Stopwatch;
