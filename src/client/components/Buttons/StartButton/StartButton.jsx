import React, { useContext } from 'react';
import context from "../../../context/context";

const StartButton = () => {
  const{startCount} = useContext(context);

  return (
      <>
        <button id="button-start" onClick={startCount}>Start</button>
      </>
  );
};

export default StartButton;
