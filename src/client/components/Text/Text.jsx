import React from 'react';
import './Text.css';

const Text = (props) => {
  const {title, subTitle} = props;
  return (
    <>
      <h1>{title}</h1>
      <h2>{subTitle}</h2>
    </>
  );
};

export default Text;
