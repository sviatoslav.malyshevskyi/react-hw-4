import React from 'react';
import './App.css';
import Provider from "./client/context/Provider";
import Text from "./client/components/Text";
import Stopwatch from "./client/components/Stopwatch";
import Buttons from "./client/components/Buttons";
import textProps from "./client/components/Text/textProps";

function App() {
  return (
      <div className="App">
        <div className="wrapper">
          <Text {...textProps} />
          <Provider>
            <Stopwatch />
            <Buttons />
          </Provider>
        </div>
      </div>
  );
}

export default App;
