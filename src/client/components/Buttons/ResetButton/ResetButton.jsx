import React, { useContext } from 'react';
import context from "../../../context/context";

const ResetButton = () => {
  const {resetCount} = useContext(context);

  return (
      <>
        <button id="button-stop" onClick={resetCount}>Reset</button>
      </>
  );
};

export default ResetButton;
