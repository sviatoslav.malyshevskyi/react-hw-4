import React, { useContext } from 'react';
import context from "../../../context/context";

const StopButton = () => {
  const {stopCount} = useContext(context);

  return (
      <>
        <button id="button-reset" onClick={stopCount}>Stop</button>
      </>
  );
};

export default StopButton;
