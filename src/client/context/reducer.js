import { START, STOP, RESET } from './constants';

const reducer = (state, {type, payload = null}) => {
  switch(type) {

    case START:
      const {sec, msec} = state;
      let secCount = sec;
      let msecCount = +msec + 1;

      if (msecCount < 1) {
        msecCount = '00' + msecCount;
      }
      if (msecCount > 99) {
        msecCount = '00';
        secCount = +secCount + 1;

        if (secCount < 1) {
          secCount = '0' + secCount;
        }
      }
      return {...state, isCounting: true, msec: msecCount, sec: secCount, interval: payload};

    case STOP:
      return {...state, isCounting: false};

    case RESET:
      return {...state, isCounting: false, sec: '00', msec: '00'};

    default: return state;
  }
};

export default reducer;
